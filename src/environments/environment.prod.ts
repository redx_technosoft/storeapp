/*
  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  App Name : ionic 5 groceryee app
  Created : 10-Sep-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.
*/
export const environment = {
  production: true,
  baseURL: 'http://api.1mandi.com/index.php/',
  mediaURL: 'http://api.1mandi.com/uploads/',
  onesignal: {
    appId: '65016e64-c5f2-496f-b171-ae90f2765732',
    googleProjectNumber: '1018030958122',
    restKey: 'NDZmYTlkMGEtYjUwZi00YWJjLWE2ODItYjE1NjRkYjJiNTY0'
  },
  general: {
    symbol: '$',
    code: 'USD'
  },
  authToken: '123456789'
};
